var express = require("express");
var router = express.Router();
const { User } = require("../models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 4;
/* GET users listing. */
router.post("/register", async (req, res) => {
  const { email, password } = req.body;
  try {
    const isEmailExist = await User.findOne({
      where: {
        email,
      },
    });
    if (isEmailExist) {
      return res.status(401).json({
        message: "email already exist",
      });
    }

    const passwordHashed = await bcrypt.hash(password, saltRounds);

    await User.create({
      email,
      password: passwordHashed,
    });
    return res.json({
      message: "Successfully create new user. Please Login",
    });
  } catch (error) {
    console.log("error", error);
    res.status(401).json({
      message: error.message,
    });
  }
});

router.post("/login", async (req, res, next) => {
  try {
    const { email, password } = req.body;
    // cari apakah ada user dengan email yang dimasukan
    const isUserFound = await User.findOne({
      where: {
        email,
      },
    });
    // kalo user gak ketemu kirim error
    if (!isUserFound) {
      return res.status(401).json({
        message: "email or password wrong",
      });
    }
    const isPasswordMatch = await bcrypt.compare(
      password,
      isUserFound.password
    );

    if (!isPasswordMatch) {
      return res.status(401).json({
        message: "email or password wrong",
      });
    }

    const token = await jwt.sign({ id: isUserFound.id }, "private");

    return res.json({
      message: "Succesfully login",
      token,
    });
  } catch (error) {
    console.log("error", error);
    res.status(401).json({
      message: error.message,
    });
  }
});

module.exports = router;
