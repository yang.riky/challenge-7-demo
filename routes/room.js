var express = require("express");
var router = express.Router();
const { PlayerRoom, UserGameHistory } = require("../models");
const jwtAuthentication = require("../middlewares/jwtAuthentication");
/* GET users listing. */

// program to generate random strings

// declare all characters
const characters = "abcdefghijklmnopqrstuvwxyz0123456789";

function generateString(length) {
  let result = " ";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return result;
}
function generateResult(yourChoice, enemyChoice) {
  if (yourChoice == enemyChoice) {
    return "draw";
  }
  if (yourChoice === "paper") {
    if (enemyChoice == "scissor") {
      return "lose";
    }
    if (enemyChoice == "rock") {
      return "win";
    }
  }
  if (yourChoice === "rock") {
    if (enemyChoice == "paper") {
      return "lose";
    }
    if (enemyChoice == "scissor") {
      return "win";
    }
  }
  if (yourChoice === "scissor") {
    if (enemyChoice == "rock") {
      return "lose";
    }
    if (enemyChoice == "paper") {
      return "win";
    }
  }
}

function getReverseResult(result) {
  if (result == "win") return "lose";
  if (result == "lose") return "win";
  return "draw";
}


// silahkan cari logic buat bikin gamenya beronde ronde.
router.post("/fight/:idRoom", jwtAuthentication, async (req, res) => {
  try {
    const isRoomExist = await PlayerRoom.findOne({
      where: {
        uniqueName: req.params.idRoom,
      },
    });
    if (!isRoomExist) {
      return res.status(401).json({
        message: "Room not exist",
      });
    }

    const isHistoryExist = await UserGameHistory.findOne({
      where: {
        roomId: isRoomExist.id,
      },
    });
    if (isHistoryExist) {
      if (isHistoryExist.userId == req.user.id) {
        res.json({
          message: "you cant suit again",
        });
      } else {
        await UserGameHistory.create({
          playerChoice: req.body.playerChoice,
          roomId: isRoomExist.id,
          userId: req.user.id,
          result: generateResult(
            req.body.playerChoice,
            isHistoryExist.playerChoice
          ),
        });
        isHistoryExist.result = getReverseResult(
          generateResult(req.body.playerChoice, isHistoryExist.playerChoice)
        );

        await isHistoryExist.save();
      }
    } else {
      await UserGameHistory.create({
        playerChoice: req.body.playerChoice,
        roomId: isRoomExist.id,
        userId: req.user.id,
      });
    }
    res.json({
      message: "Selesai Fight",
    });
  } catch (error) {
    res.json({
      message: error.message,
    });
  }
});

router.post("/join/:idRoom", jwtAuthentication, async (req, res) => {
  try {
    const isRoomExist = await PlayerRoom.findOne({
      where: {
        uniqueName: req.params.idRoom,
      },
    });
    if (!isRoomExist) {
      return res.status(401).json({
        message: "Room not exist",
      });
    }
    if (isRoomExist.challengerId) {
      return res.status(401).json({
        message: "Room Full",
      });
    }
    if (isRoomExist.ownerId == req.user.id) {
      return res.status(401).json({
        message: "You cant join your own room",
      });
    }

    isRoomExist.challengerId = req.user.id;

    isRoomExist.save();
    return res.status(200).json({
      message: "Join room",
      isRoomExist,
    });
  } catch (error) {
    res.status(401).json({
      message: error.message,
    });
  }
});
router.post("/create", jwtAuthentication, async (req, res, next) => {
  try {
    const payload = {
      name: req.body.name,
      ownerId: req.user.id,
      uniqueName: generateString(5),
    };
    console.log("req.user.id", req.user.id);
    const isPlayerHaveRoom = await PlayerRoom.findOne({
      where: {
        ownerId: req.user.id,
        roomStatus: true,
      },
    });
    if (isPlayerHaveRoom) {
      return res.status(401).json({
        message: "You have already your own room",
      });
    }
    await PlayerRoom.create(payload);
    res.json({
      message: "Succesfully create room",
    });
  } catch (error) {
    res.status(401).json({
      message: error.message,
    });
  }
});

module.exports = router;
